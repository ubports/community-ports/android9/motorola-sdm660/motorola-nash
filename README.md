# Ubuntu Touch for Motorola Z2 Force
## Instructions

### Trebelize:
Since this device came out with Android 7.1.1 originally, it is necessary "trebelize" the device to get a separate vendor partition,
where the OS will pull the device drivers from. 
Thankfully someone already did it for us:
- download [twrp](https://dl.twrp.me/nash/twrp-3.5.2_9-0-nash.img.html)
- download and flash [mokee 9.0 (MK90.0-nash-210620-HISTORY.zip)](https://download.mokeedev.com/nash/file/MK90.0-nash-210620-HISTORY.zip/download) using twrp
  - `fastboot boot twrp-3.5.2_9-0-nash.img`
  - install the downloaded mokee zip using adb sidelod

### Flash Ubuntu Touch
- download latest devel-flashable artifacts from [CI pipeline](https://gitlab.com/ubports/community-ports/android9/motorola-sdm660/motorola-nash/-/pipelines) and extract it
- flash boot and system partition using fastboot:
  - `fastboot flash boot boot.img && fastboot flash system system.img`
  - (maybe it is needed to wipe data partition with `fastboot -w`)
